#! /bin/sh

# Create needed directories
mkdir -p ~/Programs

# Install requirements
sudo pacman -Sy --noconfirm $(cat pkglist.txt)

# Lightdm
sudo pacman -S lightdm lightdm-webkit2-greeter lightdm-webkit-theme-litarvan
sudo cp lightdm/lightdm.conf /etc/lightdm/lightdm.conf
sudo cp lightdm/lightdm-webkit2-greeter.conf /etc/lightdm/lightdm-webkit2-greeter.conf

# rEFInd configuration
sudo mkdir -p /boot/EFI/BOOT/themes
cd /boot/EFI/BOOT/themes
sudo git clone https://github.com/EvanPurkhiser/rEFInd-minimal.git
cd ~/Programs/dotfiles
sudo cp refind/refind.conf /boot/EFI/BOOT/refind.conf

# Install pa-applet-git
cd ~/Programs
git clone https://aur.archlinux.org/pa-applet-git.git
cd pa-applet-git
makepkg -sri

# Install siji-git
cd ~/Programs
git clone https://aur.archlinux.org/siji-git.git
cd siji-git
makepkg -sri

# Install polybar
cd ~/Programs
git clone https://aur.archlinux.org/polybar.git
cd polybar
makepkg -sri

# Install zsh and change the default shell
# Install oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

cd ~/Programs/dotfiles

cp zsh/.zshrc ~/.zshrc

# copy the zsh theme across
cp oh-my-zsh/schminitz.zsh-theme ~/.oh-my-zsh/themes/schminitz.zsh-theme

# Create bspwm and sxhkd folders and their symlinks
mkdir -p ~/.config ~/.config/bspwm ~/.config/sxhkd ~/.config/polybar

cp bspwm/bspwmrc ~/.config/bspwm/bspwmrc
cp sxhkd/sxhkdrc ~/.config/sxhkd/sxhkdrc
cp -r polybar/* ~/.config/polybar/

chsh -s /bin/zsh
sudo systemctl enable lightdm

echo "Done!"
