#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar
killall -q pa-applet

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
polybar main-top -c $HOME/.config/polybar/config-top.ini &

external_monitor=$(xrandr --query | grep 'HDMI-1')

if [[ $external_monitor = *connected* ]]
then
	polybar secondary-top -c $HOME/.config/polybar/config-top.ini &
fi
