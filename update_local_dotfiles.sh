#! /bin/sh

printf "Copying files from repo to local configs..."
cp -f bspwm/bspwmrc ~/.config/bspwm/bspwmrc
cp -f sxhkd/sxhkdrc ~/.config/sxhkd/sxhkdrc
cp -r polybar/* ~/.config/polybar/*
cp -f zsh/.zshrc ~/.zshrc
cp -f refind/refind.conf /boot/EFI/refind/refind.conf
cp -f lightdm/lightdm.conf /etc/lightdm/lightdm.conf
cp -f lightdm/lightdm-webkit2-greeter.conf /etc/lightdm/lightdm-webkit2-greeter.conf
printf "Done!\n"
