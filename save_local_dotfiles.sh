#! /bin/sh

printf "Creating backup directories..."
# create a backup directory if its not already there
mkdir -p backup
mkdir -p backup/bspwm
mkdir -p backup/sxhkd
mkdir -p backup/polybar
mkdir -p backup/zsh
mkdir -p backup/refind
mkdir -p backup/lightdm
printf "Done!\n"

printf "Saving old remote files to backup..."
# save all current remote versions of dotfiles to backup folder
cp bspwm/bspwmrc backup/bspwm/bspwmrc
cp sxhkd/sxhkdrc backup/sxhkd/sxhkdrc
cp -r polybar/* backup/polybar/
cp zsh/.zshrc backup/zsh/.zshrc
cp refind/refind.conf backup/refind/refind.conf
cp lightdm/lightdm.conf backup/lightdm/lightdm.conf
cp lightdm/lightdm-webkit2-greeter.conf backup/lightdm/lightdm-webkit2-greeter.conf
printf "Done!\n"

printf "Saving local files to the repo..."
# save all local dotfiles to the git repo
cp ~/.config/bspwm/bspwmrc bspwm/bspwmrc
cp ~/.config/sxhkd/sxhkdrc sxhkd/sxhkdrc
cp -r ~/.config/polybar/* polybar/
cp ~/.zshrc zsh/.zshrc
cp /boot/EFI/refind/refind.conf refind/refind.conf
cp /etc/lightdm/lightdm.conf lightdm/lightdm.conf
cp /etc/lightdm/lightdm-webkit2-greeter.conf lightdm/lightdm-webkit2-greeter.conf
printf "Done!\n"
